
import xbmcgui

class notificationWindow(xbmcgui.WindowXMLDialog):

    def __init__(self, *args, **kwargs):
        self.visible = False
        self.wizTextbox = 1400
        self.wizTitle = 1399
        self.tokenText = 1401
        self.oe = kwargs['oeMain']
        self.headerWindow = 32300


    def onInit(self):
        pass


    def set_notificationWindow_text(self, text):
        try:
            self.getControl(self.wizTextbox).setText(text)
        except Exception, e:
            self.oe.dbg_log('_oewindow_::__set_notification_text__', 'ERROR: (' + repr(e) + ')')

    def set_notificationWindow_title(self, title):
        try:
            self.getControl(self.wizTitle).setLabel(title)
        except Exception, e:
            self.oe.dbg_log('set_notificationWindow_title::', 'ERROR: (' + repr(e) + ')')
    
    def set_notificationWindow_header(self, header):
        try:
            self.getControl(self.headerWindow).setLabel(header)
        except Exception, e:
            self.oe.dbg_log('set_notificationWindow_header::',
                            'ERROR: (' + repr(e) + ')')
    
    def set_notificationWindow_token(self, token):
        try:
            self.getControl(self.tokenText).setLabel(token)
        except Exception, e:
            self.oe.dbg_log('set_notificationWindow_header::',
                            'ERROR: (' + repr(e) + ')')



    def onAction(self, action):
        pass

    def onClick(self, controlID):
        pass

    def onFocus(self, controlID):
        pass
