import oeWindows
import sys
import xbmcaddon
import xbmcgui
import xbmc
import requests
import fcntl, socket, struct
import traceback
import os

__oe__ = sys.modules[globals()['__name__']]
__scriptid__ = 'service.subscription'
__addon__ = xbmcaddon.Addon(id=__scriptid__)
__cwd__ = __addon__.getAddonInfo('path')


__url__ = "https://api.crcqr.com/external/iptv/verify_subscription?mac_address={}"
# __url__ = "http://192.168.50.103:8000/iptv/verify_subscription?mac_address={}"

def _(code):
    return __addon__.getLocalizedString(code).encode('utf-8')

def dbg_log(source, text, level=4):
    if level == 0 and os.environ.get('DEBUG', 'no') == 'no':
        return
    xbmc.log('## Verify subscription Addon ## ' + source + ' ## ' + text, level)
    if level == 4:
        xbmc.log(traceback.format_exc(), level)

def openWindow():
    global winOeMain, __cwd__, __oe__
    try:
        winOeMain = oeWindows.notificationWindow('notification.xml', __cwd__, 'Default', oeMain=__oe__)
        winOeMain.visible = True
        winOeMain.doModal()
    except Exception, e:
        self.oe.dbg_log('_oe_::__openWindow__', 'ERROR: (' + repr(e) + ')')

def stop_player():
    player = xbmc.Player()
    if player.isPlaying():
        player.stop()

def getHwAddr(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s',ifname[:15]))
    return ':'.join(['%02x' % ord(char) for char in info[18:24]])

        

def verify_subscription():

    __mac_address__ = getHwAddr("eth0")
    url = __url__.format(__mac_address__)

    try:
        response = requests.get(url).json()
        return response

    except Exception as e:
        dbg_log('_oe_::__verify_subscription__', 'ERROR: (' + repr(e) + ')')



winOeMain = oeWindows.notificationWindow('notification.xml', __cwd__, 'Default', oeMain=__oe__)
