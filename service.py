# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2009-2013 Stephan Raue (stephan@openelec.tv)
# Copyright (C) 2013 Lutz Fiebach (lufie@openelec.tv)
# Copyright (C) 2018-present Team CoreELEC (https://coreelec.org)

import oe
import xbmc
import time
import threading

class service_thread(threading.Thread):

    def __init__(self, oeMain):
        try:
            self.oe = oeMain
            self.wait_evt = threading.Event()
            self.stopped = False
            threading.Thread.__init__(self)
            self.daemon = True

        except Exception, e:
            self.oe.dbg_log('_service_::__init__', 'ERROR: (' + repr(e) + ')')

    def stop(self):
        try:
            self.stopped = True
        except Exception, e:
            self.oe.dbg_log('_service_::__stop__', 'ERROR: (' + repr(e) + ')')

    def run(self):
        try:
            while True:
                try:
                    response = self.oe.verify_subscription()
                    if 'status' in response:
                        if response["status"] == "not_assocciate":
                            if self.oe.winOeMain.visible == False:
                                threading.Thread(target=self.oe.openWindow).start()
                                self.oe.winOeMain.visible = True
                            self.oe.winOeMain.set_notificationWindow_text(self.oe._(603))
                            self.oe.winOeMain.set_notificationWindow_header(self.oe._(601))
                            self.oe.winOeMain.set_notificationWindow_token(response["token"])
                            self.oe.winOeMain.set_notificationWindow_title(self.oe._(602))
                            self.oe.stop_player()
                        elif response["status"] == "not_payed":
                            if self.oe.winOeMain.visible == False:
                                threading.Thread(target=self.oe.openWindow).start()
                                self.oe.winOeMain.visible = True
                            self.oe.winOeMain.set_notificationWindow_token("")
                            self.oe.winOeMain.set_notificationWindow_text(self.oe._(605))
                            self.oe.winOeMain.set_notificationWindow_header(self.oe._(600))
                            self.oe.winOeMain.set_notificationWindow_title(self.oe._(602))
                            self.oe.stop_player()
                        elif response["status"] != "active":
                            if self.oe.winOeMain.visible == False:
                                threading.Thread(target=self.oe.openWindow).start()
                                self.oe.winOeMain.visible = True
                            self.oe.winOeMain.set_notificationWindow_text(
                                response["status"])
                            self.oe.winOeMain.set_notificationWindow_title(
                                self.oe._(602))
                            self.oe.stop_player()
                        elif response["status"] == "active" and self.oe.winOeMain.visible == True:
                            self.oe.winOeMain.close()
                            self.oe.winOeMain.visible = False
                            
                    else:
                        active = response["status"]
                        if active == "active" and self.oe.winOeMain.visible == True:
                            self.oe.winOeMain.close()
                            self.oe.winOeMain.visible = False
                    time.sleep(600) #cada 10 minutos
                except Exception, e:
                    self.oe.dbg_log('_service_::__run__','ERROR: (' + repr(e) + ')')
                    time.sleep(600)  # cada 10 minutos
        except Exception, e:
            self.oe.dbg_log('_service_::__run__', 'ERROR: (' + repr(e) + ')')



class cxbmcm(xbmc.Monitor):

    def __init__(self, *args, **kwargs):
        xbmc.Monitor.__init__(self)

    def onScreensaverActivated(self):
        pass

    def onAbortRequested(self):
        pass


xbmcm = cxbmcm()
monitor = service_thread(oe.__oe__)
monitor.start()

xbmcm.waitForAbort()
monitor.stop()
